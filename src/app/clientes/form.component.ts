import { Component, OnInit } from '@angular/core';
import { Cliente } from './cliente';
import { ClienteService } from './cliente.service';
import { Router, ActivatedRoute } from '@angular/router';
import swal from 'sweetalert2';

@Component({
  selector: 'app-form',
  templateUrl: './form.component.html',
  styleUrls: ['./form.component.css']
})
export class FormComponent implements OnInit {
  private cliente: Cliente = new Cliente()
  private titulo:string = "Crear Cliente"
  private errores: string[]; //atributo para manejar errores

  constructor(private clienteService: ClienteService,
  private router: Router,
  private ActivatedRoute: ActivatedRoute) { }

  ngOnInit() {
    this.cargarCliente()
  }

  cargarCliente(): void{
    this.ActivatedRoute.params.subscribe(params =>{
      let id = params['id']
      if(id){
        this.clienteService.getCliente(id).subscribe(
          (cliente) => this.cliente = cliente)
      }
    })
  }

  public create(): void{
    this.clienteService.create(this.cliente).subscribe(
      cliente => {
      this.router.navigate(['/clientes']) //redireccionamos
      swal.fire('Nuevo cliente',`Cliente  ${cliente.nombre} ha sido actualizado con exito`,'success') //json contiene el nombre
    },
    err => {
      this.errores =err.error.errors as string[];
      console.error('Código del error desde el backend: ' + err.status);
      console.error(err.error.errors);
    }
    )
  }

  update():void{
    this.clienteService.update(this.cliente).subscribe( cliente =>
      {this.router.navigate(['/clientes'])
      swal.fire('Cliente Actualizado',`${cliente.mensaje}: ${cliente.nombre}`,'success')
    },
    err => {
      this.errores =err.error.errors as string[];
      console.error('Código del error desde el backend: ' + err.status);
      console.error(err.error.errors);
    }
    )
  }



}
