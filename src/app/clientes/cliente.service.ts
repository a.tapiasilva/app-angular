import { Injectable } from '@angular/core';
import { CLIENTES } from './clientes.json';
import { Cliente } from './cliente';
import { HttpClient,HttpHeaders } from '@angular/common/http';
import { of, Observable, throwError } from 'rxjs';
import { map, catchError } from 'rxjs/operators';
import swal from 'sweetalert2';
import { Router } from '@angular/router';


@Injectable()

export class ClienteService {
  private urlEndpoint:string = "http://localhost:8080/api/clientes"

  private httpHeaders = new HttpHeaders({'Content-Type':'application/json'})
  constructor(private http: HttpClient, private router: Router) { } //inyectamos HttpCliente y Router en el constructor

  getClientes(): Observable<Cliente[]>{
    return this.http.get<Cliente[]>(this.urlEndpoint).pipe( //invocamos a ClienteRestController
      map(response => response as Cliente[])
    );
    //return of(CLIENTES);
  }

  //metodo create con manejo de error
  create(cliente: Cliente) :  Observable<Cliente>{ //si se retorna any para cualquier tipo de dato, buscando flexibilidad
    return this.http.post(this.urlEndpoint, cliente,{headers: this.httpHeaders}).pipe( //invocamos a ClienteRestController a traves de metodo pipe
      map((response: any) => response.cliente as Cliente),
      catchError(e => { //recibe por argumento objeto e
        if(e.status==400){
          return throwError(e);
        }
          console.error(e.error.mensaje); //Muestra error en consola del navegador
          swal.fire(e.error.mensaje, e.error.error,'error'); //muestra error en alert capturando error desde el back
          return throwError(e);//retorna objeto e error en tipo observable.
      }) //capturando errores
    );
  }


  getCliente(id): Observable<Cliente>{
    return this.http.get<Cliente>(`${this.urlEndpoint}/${id}`).pipe(
      catchError(e => {
        this.router.navigate(['/clientes']); //capturamos y redireccionamos a clientes.
        console.error(e.error.mensaje); //imprime error en consola
          swal.fire('Error al editar',e.error.mensaje,'error'); //mostramos error en alert capturando mensaje de error desde el back, 'error' se ocupa para dar formato error.
          return throwError(e);  //convierte error en tipo observable
        })
    );
  }


  //metodo actualizar
  update(cliente: Cliente): Observable<any>{
    return this.http.put<any>(`${this.urlEndpoint}/${cliente.id}`,cliente,{headers: this.httpHeaders}).pipe(
      catchError(e => { //capturamos errores en objeto e a través de catchError
        if(e.status==400){
          return throwError(e);
        }
        console.error(e.error.mensaje);
        swal.fire(e.error.mensaje,e.error.error,'error');
        return throwError(e); //convierte error en tipo observable
      })
    )
  }


  //metodo actualizar
  delete(id: number): Observable<Cliente>{
    return this.http.delete<Cliente>(`${this.urlEndpoint}/${id}`,{headers: this.httpHeaders}).pipe(
      catchError(e => {
        console.error(e.error.mensaje);
        swal.fire(e.error.mensaje,e.error.error,'error');
        return throwError(e);
      })
    )
  }


}
