import { Component } from '@angular/core';

@Component({
  selector: 'app-directiva',
  templateUrl: './directiva.component.html'
})
export class DirectivaComponent {


  listaCurso: string[] = ['TypeScript','JavaScript','JAVA SE','C#','Ruby'];
  habilitar: boolean = true; //mostrar listado de curso

  constructor() { }
  setHabilitar(): void {
    this.habilitar = (this.habilitar==true)? false: true;
  }

}
